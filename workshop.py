# 1
def check_palindrome(x):
    if x == x[::-1]:
        return True
    else:
        return False


# 2
def change(x):
    coins = 0
    while True:
        if x >= 50:
            coins += x // 50
            x -= 50 * (x // 50)

        elif x >= 20:
            coins += x // 20
            x -= 20 * (x // 20)

        elif x >= 10:
            coins += x // 10
            x -= 10 * (x // 10)

        elif x >= 5:
            coins += x // 5
            x -= 5 * (x // 5)

        else:
            coins += x
            return coins


# 3
def check_parentheses(x):
    y = ['(', ')']
    if x[0] == y[0] and len(x) % 2 == 0:
        x = list(x)
        while len(x) != 2:
            del (x[0])
            del (x[-1])
        if x[0] != x[1]:
            return True
        else:
            return False
    else:
        return "False"


# 4
def steps(x):
    if 0 <= x <= 3:
        return x
    elif x < 0:
        return None
    else:
        return steps(x - 1) + steps(x - 2)


# 5.1
import sqlite3

conn = sqlite3.connect("db.sqlite")
cur = conn.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS teacher
                    (firstname VARCHAR(50),
                        lastname VARCHAR(100),
                         sex VARCHAR(100),
                        subject VARCHAR(100));''')

cur.execute('''CREATE TABLE IF NOT EXISTS pupil
                    (firstname VARCHAR(50),
                        lastname VARCHAR(100),
                         sex VARCHAR(100),
                        class INTEGER);''')


# 6
import requests
import json
import sqlite3
from flask import Flask

url = 'https://api.chucknorris.io/jokes/random'

conn = sqlite3.connect('test.db', check_same_thread=False)
c = conn.cursor()
c.execute("CREATE TABLE IF NOT EXISTS jokes (id varchar(200), joke varchar (255))")
app = Flask(__name__)


@app.route('/random_joke')
def random_joke():
    res = requests.get(url)
    r = res.json()

    with open('joke.txt', 'a') as f:
        f.write(r['value'])
        f.write('\n')

    c.execute("insert into jokes values (?,?)", [r['id'], r['value']])
    conn.commit()

    return r['value']


@app.route('/saved_jokes')
def load_joke():
    c.execute('select * from jokes')
    t = c.fetchall()
    keys = ['id', 'joke']
    data = [dict(zip(keys, i)) for i in t]
    json_data = json.dumps(data, indent=4)
    return json_data


if __name__ == '__main__':
    app.run(debug=True)
    conn.close()
